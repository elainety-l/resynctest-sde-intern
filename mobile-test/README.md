# note_app

A new Flutter project.

1. Note Tab: User can Add/Edit/Delete notes like, Text, bullet points. 
   When user save/update timestamp should be updated and based on that Older notes from current time should be display in history tab and remove
   note tab.
2. Alarm Tab: User can Add/Edit/Delete Alarm/reminder with Date and time (Future time only). When the configured time comes application should notify user
   by notification/banner pop-up. Alarms/reminders which are older than current time should be display in history tab and remove from alarm tab.
3. History Tab: History tab only shows past notes and alarms. User can only delete in history tab. Cannot Add new or Edit existing.



## Getting Started

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://flutter.dev/docs/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://flutter.dev/docs/cookbook)

For help getting started with Flutter, view our
[online documentation](https://flutter.dev/docs), which offers tutorials,
samples, guidance on mobile development, and a full API reference.
